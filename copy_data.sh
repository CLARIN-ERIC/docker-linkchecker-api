#!/bin/bash

# shellcheck source=copy_data.env.sh
source "${DATA_ENV_FILE:-copy_data.env.sh}"


init_data (){
   LOCAL_GIT_DIRECTORY="$(pwd)/application-src"
    
   cleanup_data

   if [ -z "${DEV_LOCATION}" ]; then     
      mkdir -p "${LOCAL_GIT_DIRECTORY}"
        (
         TMP_TGZ=$(mktemp)
         cd "${LOCAL_GIT_DIRECTORY}" && 
            wget -O "${TMP_TGZ}" "${GIT_REPO_URL}/archive/${GIT_BRANCH}.tar.gz" &&
            tar zxvf "${TMP_TGZ}" --strip-components 1 &&
            rm "${TMP_TGZ}"
        )
    else
        cp -r "$DEV_LOCATION" "$LOCAL_GIT_DIRECTORY"
    fi  
}

cleanup_data () {
    if [ -d "${LOCAL_GIT_DIRECTORY}" ]; then
      echo "Cleaning up source directory ${LOCAL_GIT_DIRECTORY}"
       rm -rf "${LOCAL_GIT_DIRECTORY}"
   fi
}
